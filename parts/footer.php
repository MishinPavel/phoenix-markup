<footer>
	<div class="container clearfix">
		<div class="logo">
			<a href="/" class="logo--footer">
				<img src="/img/home/logo-small-desktop.png" alt="phoenix">
			</a>
		</div>
		<div class="conditionals">
			&copy; 2017 Phoenix. All Rights Reserved.
			<span>Privacy Policy</span>
		</div>
		<div class="socials">
			<a href="#">
				<i class="fa fa-facebook" aria-hidden="true"></i>
			</a>
			<a href="#">
				<i class="fa fa-google-plus" aria-hidden="true"></i>
			</a>
			<a href="#">
				<i class="fa fa-twitter" aria-hidden="true"></i>
			</a>
			<a href="#">
				<i class="fa fa-linkedin" aria-hidden="true"></i>
			</a>
		</div>
	</div>

</footer>

<script src="https://use.typekit.net/ptl8fyx.js"></script>
<script>try {
        Typekit.load(
            {async: true}
        );
    } catch (e) {
    }</script>
<script src="js/jquery.js"></script>
<script src="js/script.js"></script>

</body>
</html>