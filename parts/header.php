<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>Phoenix</title>

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

</head>

<body>

	<header>
		<div class="container">
			<div class="logo">
				<a href="/" class="logo--header">
					<img src="img/home/logo-desktop.png" alt="phoenix">
				</a>
			</div>
			<nav class="header-nav">
				<ul>
					<li class="header-nav__item">
						<a href="/work.php">WORK</a>
					</li>
					<li class="header-nav__item">
						<a href="#">SOLUTIONS</a>
					</li>
					<li class="header-nav__item">
						<a href="#">LEARN</a>
					</li>
					<li class="header-nav__item">
						<a href="#">ABOUT</a>
					</li>
					<li class="header-nav__item">
						<a href="#">CONTACT</a>
					</li>
					<li class="header-nav__item">
						<a href="#">SHOP</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>
