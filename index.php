
	<?php include( 'parts/header.php' ); ?>

	<main class="front-page">
		<div class="container">

			<section class="slider">
				<a href="#" class="slider__control slider__control--prev fa fa-chevron-left"></a>
				<a href="#" class="slider__control slider__control--next fa fa-chevron-right"></a>
				<ul class="slider-content">
					<li class="slide slider-content__slide">
						<h2 class="slide__header">Report: 2017 Hotel Scores Study</h2>
						<p class="slide__text">
							New data that measures customer perceptions and usage of 38 Hotel Frequent Guest Programs
							offers insight into gusts preferences.
							<a href="#" class="slide__link">READ THE REPORT</a>
						</p>
					</li>
					<li class="slider-content__slide">

					</li>
					<li class="slider-content__slide">

					</li>
					<li class="slider-content__slide">

					</li>
					<li class="slider-content__slide">

					</li>
					<li class="slider-content__slide">

					</li>
				</ul>
				<div class="slider-controls">
					<div class="slider-controls__dot"></div>
					<div class="slider-controls__dot"></div>
					<div class="slider-controls__dot slider-controls__dot--active"></div>
					<div class="slider-controls__dot"></div>
					<div class="slider-controls__dot"></div>
					<div class="slider-controls__dot"></div>
				</div>
			</section>

			<section class="highlighted">
				<h2 class="highlighted__header">Research – Reborn.</h2>
				<p class="highlighted__text">When you work with Phoenix, you get insights that impact your business. You
					get creative solutions. You get guidance. All based on the highest quality data, but never just
					data.</p>
				<p class="highlighted__text">Bottom line? We help you succeed…</p>
			</section>

			<section class="main-about-us">
				<div class="tabs tabs--front">
					<div class="tab tab--active">Our Mission</div>
					<div class="tab">Leadership</div>
				</div>

				<div class="tabs-content">
					<section class="tab-content">
						<h2 class="tab-content__header">SOLUTIONS? WE’VE GOT THEM.</h2>
						<p class="tab-content__description">
							Our solutions in Ad and Brand Communications, <br>
							Customer Experience, and Custom Analytics enable you to set differentiating customer
							expectations, understand how you are delivering against those expectations and
							understand how to win in the marketplace
						</p>

						<div class="flex-row">
							<div class="flex-col">
								<article class="main-about-us__item">
									<img src="img/home/our-mission1.jpg">
									<h3>HEADER SECTION</h3>
									<p>
										Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod corpora euea
										mel saperet ullamcorper mutat animal</p>
								</article>
							</div>
							<div class="flex-col">
								<article class="main-about-us__item">
									<img src="img/home/MadeWith_hackathon.jpg">
									<h3>HEADER SECTION</h3>
									<p>
										Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod corpora euea
										mel saperet ullamcorper mutat animal</p>
								</article>
							</div>
							<div class="flex-col">
								<article class="main-about-us__item">
									<img src="img/home/our-mission3.jpg">
									<h3>HEADER SECTION</h3>
									<p>
										Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod corpora euea
										mel saperet ullamcorper mutat animal</p>
								</article>
							</div>
						</div>
					</section>
				</div>
			</section>

			<section class="main-news">
				<h2>IN THE NEWS</h2>

				<section class="news flex-row">


					<div class="flex-col flex-col--big">

						<div class="news-item news-item--big">

							<img src="img/home/news_big.jpg" width="447" alt="">

							<div class="news-item__preview">
								<h3 class="preview-header">Data May Be the Holy Grail, But Creative Is the Secret
									Weapon</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper </p>
							</div>

						</div>

					</div>

					<div class="flex-col flex-coll--small">

						<div class="news-item news-item--regular">

							<img src="img/home/news1.jpg" alt="">

							<div class="news-item__preview">
								<h3 class="preview-header">PERSON TALKS AT TEDX PANEL</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper </p>
							</div>

						</div>

						<div class="news-item news-item--regular">

							<img src="img/home/news2.jpg" alt="">

							<div class="news-item__preview">
								<h3 class="preview-header">BLOOMBERG THE FUTURE OF WORK
								</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper
								</p>
							</div>

						</div>

					</div>

					<div class="flex-col flex-coll--small">
						<div class="news-item news-item--regular">

							<img src="img/home/news3.jpg" alt="">

							<div class="news-item__preview">
								<h3 class="preview-header">Campaigns Are Dead. Modern Marketing Is a Data Exchange</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore </p>
							</div>

						</div>

						<div class="news-item news-item--regular">

							<img src="img/home/news4.jpg" alt="">

							<div class="news-item__preview">
								<h3 class="preview-header">NAVIGATING DIVERSITY: THENEW NORMAL</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore </p>
							</div>

						</div>
					</div>

				</section>

				<h2>UPCOMING EVENTS</h2>

				<section class="events flex-row">


					<div class="flex-col">

						<div class="event">

							<img src="img/home/events1.jpg" alt="">

							<div class="event__date">AUG 2 2017</div>

							<div class="event__preview">
								<h3 class="preview-header">JOHN HARTMAN GIVES RESEARCH PRESENTATION AT PBIRG ANNUAL
									MEETING</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper </p>
							</div>
						</div>
					</div>

					<div class="flex-col">

						<div class="event">

							<img src="img/home/events2.jpg" alt="">

							<div class="event__date">AUG 2 2017</div>

							<div class="event__preview">
								<h3 class="preview-header">PHOENIX TO SPONSOR 2017
									MEDIA INSIGHTS & ENGAGEMENT CONFERENCE</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper meiridens.</p>
							</div>

						</div>

					</div>

					<div class="flex-col">

						<div class="event">

							<img src="img/home/events3.jpg" alt="">

							<div class="event__date">AUG 2 2017</div>

							<div class="event__preview">
								<h3 class="preview-header">JOIN US FOR A WEBINAR</h3>
								<p class="preview-text">Lorem ipsum dolor sit amet, vix enim labore mutat animal euismod
									corpora euea mel
									saperet ullamcorper meiridens.</p>
							</div>

						</div>
					</div>

				</section>
			</section>

			<section class="connect">
				<h3 class="connect--header">Want to be a marketing leader and winner? Put us to work for you!</h3>
				<a href="#" class="btn">CONNECT</a>
			</section>

			<div class="main-bottom">
				<div class="top-btn">
					<span>TOP</span> <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
				</div>
			</div>
		</div>
	</main>

	<?php include( 'parts/footer.php' ); ?>
